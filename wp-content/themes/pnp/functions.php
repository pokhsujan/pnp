<?php
/**
 * Pals and partners customizations
 *
 *
 * @package PNP
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'PNP_VERSION', '1.0.0' );

//$button_text = '', $button_options = '', $button_style = ''
function astra_get_custom_button( $button_text = '', $button_options = '', $button_style = '' ) {

	$custom_html    = '';
	$button_classes = '';
	$button_text    = astra_get_option( $button_text );
	$button_link    = astra_get_option( $button_link );
	$button_style   = astra_get_option( $button_style );
	$outside_menu   = astra_get_option( 'header-display-outside-menu' );

	$button_classes = ( 'theme-button' === $button_style ? 'ast-button' : 'ast-custom-button' );

	$outside_menu_item = apply_filters( 'astra_convert_link_to_button', $outside_menu );

	$header_button = astra_get_option( $button_options );


	global $current_user; wp_get_current_user();



	if ( '1' == $outside_menu_item ) {
		if( is_user_logged_in() ){
			$u_name = $current_user->display_name;
			$custom_html = '<a class="ast-custom-button-link" href="' . site_url("/my-account") . '"><button class=' . esc_attr( $button_classes ) . '>' . esc_attr( $current_user->display_name ) . '</button></a>';
		} else{
			//$custom_html = '<a class="ast-custom-button-link" href="' . esc_url( do_shortcode( $button_link ) ) . '"><button class=' . esc_attr( $button_classes ) . '>' . esc_attr( do_shortcode( $button_text ) ) . '</button></a>';
			$custom_html  = '<a class="ast-custom-button-link" href="' . site_url("/my-account") . '"><button class=' . esc_attr( $button_classes ) . '>' . esc_attr( do_shortcode( $button_text ) ) . '</button></a>';
			//$custom_html .= '<a class="menu-link" href="' . esc_url( do_shortcode( $header_button['url'] ) ) . '">' . esc_attr( do_shortcode( $button_text ) ) . '</a>';
		}
	} else {
		if( is_user_logged_in() ){
			$u_name = $current_user->display_name;
			$custom_html  = '<a class="ast-custom-button-link" href="' . esc_url( site_url("/my-account") ) . '"><button class=' . esc_attr( $button_classes ) . '>' . esc_attr( $current_user->display_name ) . '</button></a>';
			$custom_html .= '<a class="menu-link" href="' . esc_url( site_url("/my-account") ) . '">' . esc_attr( $current_user->display_name ) . '</a>';
		} else{
			$custom_html  = '<a class="ast-custom-button-link" href="' . site_url("/my-account") . '"><button class=' . esc_attr( $button_classes ) . '>' . esc_attr( do_shortcode( $button_text ) ) . '</button></a>';
		}
	}

	return $custom_html;
}


if ( ! function_exists( 'astra_get_prop' ) ) :

	/**
	 * Get a specific property of an array without needing to check if that property exists.
	 *
	 * 
	 */
	function astra_get_prop( $array, $prop, $default = null ) {

		if ( ! is_array( $array ) && ! ( is_object( $array ) && $array instanceof ArrayAccess ) ) {
			return $default;
		}

		if ( isset( $array[ $prop ] ) ) {
			$value = $array[ $prop ];
		} else {
			$value = '';
		}

		return empty( $value ) && null !== $default ? $default : $value;
	}

endif;


// echo apply_filters( 'wc_empty_cart_message', __( 'You have not selected any services.', 'woocommerce' ) ;	

function pnp_empty_service_message() {
    return "You have not selected any services";
}
add_filter( 'wc_empty_cart_message', 'pnp_empty_service_message', 10, 3 );



function pnp_scripts() {

	wp_enqueue_style( 'pnpc-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), PNP_VERSION, 'all' );

}
add_action( 'wp_enqueue_scripts', 'pnp_scripts', 15 );


add_filter('show_admin_bar', '__return_false');
// do_action( 'user_registration_after_login_form' )
add_action('user_registration_after_login_form', 'pnp_user_registration');
function pnp_user_registration(){
	if( !is_user_logged_in() ){
		ob_start(); ?>
		<div class="pnp-register">
			<h4>Don't Have account??</h4>
			<p class="user-registration-LostPassword lost_password">
				<a href="<?php echo site_url('registration');?>">Register Now</a>
			</p>
			
		</div>
		<?php echo ob_get_clean();
	}
}


add_filter( 'gettext', 'change_pnp_product_text', 99, 3 );
function change_pnp_product_text( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
		case 'Product' :
			$translated_text = __( 'Service', 'woocommerce' );
			break;
		case 'Products' :
			$translated_text = __( 'Services', 'woocommerce' );
			break;
		case 'product' :
			$translated_text = __( 'service', 'woocommerce' );
			break;
		case 'products' :
			$translated_text = __( 'services', 'woocommerce' );
			break;
	}
    return $translated_text;
}


add_filter( 'woocommerce_register_post_type_product', 'pnp_service_type' );
function pnp_service_type( $args ){
$labels = array(
'name' => __( 'Services', 'pnp' ),
'singular_name' => __( 'Service', 'pnp' ),
'menu_name' => _x( 'Services', 'Admin menu name', 'pnp' ),
'add_new' => __( 'Add Service', 'pnp' ),
'add_new_item' => __( 'Add New Service', 'pnp' ),
'edit' => __( 'Edit', 'pnp' ),
'edit_item' => __( 'Edit Service', 'pnp' ),
'new_item' => __( 'New Service', 'pnp' ),
'view' => __( 'View Service', 'pnp' ),
'view_item' => __( 'View Service', 'pnp' ),
'search_items' => __( 'Search Service', 'pnp' ),
'not_found' => __( 'No Service found', 'pnp' ),
'not_found_in_trash' => __( 'No Service found in trash', 'pnp' ),
'parent' => __( 'Parent Service', 'pnp' )
);
 
$args['labels'] = $labels;
$args['description'] = __( 'Pals and Partners Services.', 'pnp' );
return $args;
}


add_filter( 'woocommerce_taxonomy_args_product_cat', 'pnp_service_category_label' );
function pnp_service_category_label( $args ) {
	$args['label'] = __( 'Categories', 'pnp' );
	$args['labels'] = array(
        'name' 				=> __( 'Service Categories', 'pnp' ),
        'singular_name' 	=> __( 'Service Category', 'pnp' ),
        'menu_name'			=> __( 'Catagories', 'pnp' )
	);
	return $args;
}

add_action('admin_head', 'pnp_admin_styles');
function pnp_admin_styles() {
  echo '<style type="text/css">
	#footer-left #footer-thankyou{
		display: none;
	}
</style>';
}